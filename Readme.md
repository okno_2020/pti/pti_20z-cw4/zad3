# PTI Ćwiczenie 4, zadanie 3

## prezentacja

* [źrodło strony napisane w markdown](templateMN.md)
* [prezentacja w PDF](prezentacja.pdf)

## wymagane oprogramowanie

- [miktex](https://miktex.org/download)
- [pandoc](https://pandoc.org/)

## konwertowanie

`pandoc templateMN.md -t beamer -o prezentacja.pdf`

\*wykonanie polecenia zamuje sporo czasu