---
author: Maciej Ptasiński
title: KJS
subtitle: Amatorskie Rajdy
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Czym jest KJS

\textcolor{blue}{KJS - Konkursowa Jazda Samochodem} to impreza motoryzacyjna, zrzeszająca kierowców-amatorów chcących pościgać się własnym autem z innymi zawodnikami o najlepszy czas.

Najważniejsze informacje o __KJS-ach__ można znaleźć w **lokalnym automobilklubie.** Do klubu warto się zapisać, by stawiać kolejne kroki w sportach samochodowych.

## Jak zacząć?

Aby zacząć zabawe w __Rajdy__ wystarczy spełnić kilka podstawowych kryteriów:

* prawo jazdy to podstawa
* ubezpieczenie OC oraz NNW
* sprawny samochód

## Samochód

Niezbędnym elementem jest oczywiście samochód.
Jeśli to Twoje początki - zacznij od niedrogiego samochodu, który łatwo i tanio naprawisz.

Dobrym przykładem jest np:

* Opel Astra, 
* Fiat Seicento
* Honda Civic
* VW Golf

![Opel Astra F - GSI](assets/img/astra.jpg){ height=50% width=50%}


## Trening

Gdy już zdobędziesz upragniony samochód, warto przed samymi zawodami pomyśleć o treningu.

Na początek najlepiej znaleźć plac, na którym można poćwiczyć podstawowe rzeczy, jak np: __szybkie ruszanie__, __zmianę biegów__ czy - bardzo przydatne w późniejszych rajdach, jeśli posiadamy samochód z napędem na przednie koła - __hamowanie lewą nogą__

Przydatną umiejętnością jest również poznanie, w jaki sposób __samochód zachowuje się w zakrętach__ przy różnym __promieniu skrętu__ oraz prędkościach. Pozwoli nam to określić limity przyczepności naszego auta.

## Wymogi bezpieczeństa

Możemy wystartować całkowicie seryjnym samochodem, o ile posiada oryginalnie homologowane pasy i fotele. Jedynym kosztem, jaki musimy ponieść na początku to koszt gaśnicy oraz apteczki.

Warto również zabezpieczyć samego siebie - tu wystarczy nam kask oraz rękawiczki sportowe.

## Koszt zabawy

Pewnie wiele osob mogłoby pomyśleć, że taka zabawa jest bardzo droga.
I tak i nie, próg wejścia nie jest bardzo wysoki. 

Najtańszy nadający się do rajdów samochód można kupić już nawet za 1000 zł. Seicento jest bardzo dobrym tego przykładem. 

Podsumowując - aby zacząć zabawę wystarczy nam:

| | |
| :-- | --: |
| samochód | 1000 zł |
| gaśnica | 100 zł |
| apteczka | 50 zł |
| kask | 250 zł |
| rękawiczki | 100 zł |

Całość powinna zmieścić się do kwoty: 

\centering
\fcolorbox{white}{blue!30}{\parbox{2cm}{\centering
1500zł}}

## podsumowanie

KJS to super zabawa i nie potrzeba na nią ogromnego worka pieniędzy.

Serdecznie polecam każdemu!

![Kajetan Kajetanowicz](assets/img/kajetan-kajetanowicz.jpg){ height=60% width=60%}